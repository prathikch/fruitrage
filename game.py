import copy
import math
import time
import cProfile

def apply_gravity(board):
	"""
	:param board: a game board in which there may be empty spaces.
	:return board: a game board with gravity applied.
	"""
	#Fill all the empty places by moving the fruits above them.
	length = len(board)
	index = [ length-2 for i in board]
	for i in range( length-1, -1, -1):
		for j in range(0, length):
			if index[j] == -1:
				continue
			if board[i][j] == '*':
				if board[index[j]][j] == '*':
					index[j] -= 1
					while (board[index[j]][j] == '*') and (index[j] > -1):
						index[j] -= 1
				if index[j] > -1:
					board[i][j] = board[index[j]][j]
					board[index[j]][j] = '*'
					index[j] -= 1
			index[j] = min(index[j], i-1)
	return board

def print_board(new_board):
	"""
	Print the game board on stdout. For debug purpose only.
	:param new_board:
	:return: None
	"""
	for row in range(0, len(new_board)):
		for column in range(0, len(new_board)):
			print " ",new_board[column][row],
		print ""


def key_func(tuple1, tuple2):
	"""
	A key function for sorting in the order of highest to lowest
	points scored by the max player.
	:param tuple1: move1
	:param tuple2: move2
	"""
	return tuple2[1] - tuple1[1]


def game_over(board):
	"""
	Check if the game is over. If there is at least one non-'*' element,
	then the game is not over.
	:param board: a game board
	:return: True or False
	"""
	for row in board:
		for elem in row:
			if elem != '*':
				return False
	return True

def eval(board, maxpoints, minpoints, max):
	"""
	Evaluate a game board to see how favourable it is to the max
	player.
	:param board: a game board.
	:param maxpoints: points scored by max so far.
	:param minpoints: points scored by min so far.
	:param max: Player playing - True is MAX is playing. False if MIN.
	:return: an evaluation.
	"""
	moves = list(get_possible_moves(board))
	moves.sort(key_func)
	points_max, points_min = 0,0
	for (move, points) in moves:
		if max:
			points_max += points
			max = False
		elif not max:
			points_min += points
			max = True
	maxpoints = maxpoints + points_max
	minpoints = minpoints + points_min
	return maxpoints - minpoints

def max_move(board, minpoints, maxpoints, alpha, beta, level, cutoff):
	"""
    The max function that tries to maximize the minimum points scored by
     max.
    :param board: a game board.
    :param minpoints: points scored by MIN player so far.
    :param maxpoints: points scored by MAX player so far.
    :param alpha: the best value we have for minimum possible points so
    far.
    :param beta: the worst value we have for maximum possible points so
    far.
    :param level: the current depth of the search tree.
    :param cutoff: cutoff to trigger eval incase we are unable to search
    the entire tree.
    :return: the best possible value for MAX player.
	"""
	if game_over(board) :
		return (maxpoints - minpoints)

	elif level >= cutoff:
		return eval(board, maxpoints, minpoints, True)

	v = -1000000
	moves = list(get_possible_moves(board))
	moves.sort(key_func)
	for move, points in moves:
		gravity_move = apply_gravity(move)
		p = min_move(gravity_move, minpoints,
                     maxpoints+points , alpha, beta, level+1, cutoff)
		v = max(v,p)
		if v >= beta:
			return v
		alpha = max(v, alpha)
	return v

def min_move(board, minpoints, maxpoints, alpha, beta, level, cutoff):
	"""
    The min function that tries to minimize the maximum points scored by
    max.
    :param board: a game board.
    :param minpoints: points scored by MIN player so far.
    :param maxpoints: points scored by MAX player so far.
    :param alpha: the best value we have for minimum possible points so
    far.
    :param beta: the worst value we have for maximum possible points so
    far.
    :param level: the current depth of the search tree.
    :param cutoff: cutoff to trigger eval incase we are unable to search
    the entire tree.
    :return: the best possible value for MIN player.
	"""
	if game_over(board):
		return (maxpoints - minpoints)

	elif level >= cutoff:
		return eval(board, maxpoints, minpoints, False)

	v = 1000000
	moves = list(get_possible_moves(board))
	moves.sort(key_func)
	for move, points in moves:
		gravity_move = apply_gravity(move)
		p = max_move(gravity_move, minpoints+points,
                     maxpoints, alpha, beta, level+1, cutoff)
		v = min(v,p)
		if v <= alpha:
			return v
		beta = min(v, beta)
	return v

def calculate_bf(moves, depth):
	"""
    Calculate the approximate branching factor of our tree.
    This information is useful in evaluating the depth we go to.
    :param moves: total number of moves possible initially.
    :param depth: the depth of the tree
    :return: approximate value of average branching after for depth
	"""
	product = 1
	total_moves = moves
	for i in range(0, depth - 1):
		product *=  total_moves
		total_moves -= 1
		if total_moves<= 0:
			break
	branching_factor = 1

	for i in range(0, depth-1):
		branching_factor = total_moves*(branching_factor) + 1
		total_moves += 1
	branching_factor = total_moves*(branching_factor) +1
	branching_factor = branching_factor/float(product)
	
	return branching_factor


def get_cutoff(moves, time, speed):
	"""
	Get the cutoff/depth till which we go to in the search.
	:param moves: the total number of moves that can be made
	on the current board
	:param time: time we have left in the game
	:param speed: speed of the computer we are on.
	:return: cutoff/depth of the search tree.
	"""
	if moves <=0:
		return 0
	elif time < 0.1:
		return 0
	#If we have a lot of moves, then take all of them into account.
	#Iteratively increase the depth and have the estimate of how long it
	#  takes us to go to that depth. This depends on the branching factor.
	elif moves >= 40:
		time_per_move = time/(float(moves))
		depth = 0
		bf = calculate_bf(moves, depth)
		total_time = math.pow((float(4)*bf/5), depth)/speed
		if total_time <= 5:
			total_time = total_time +3
		elif total_time > 5:
			total_time += 5
		tmp_depth = 0
		while total_time < time_per_move:
			depth = tmp_depth
			tmp_depth +=1
			bf = calculate_bf(moves, tmp_depth)
			total_time = math.pow((float(4)*bf/5), tmp_depth)/speed

			if (total_time <= 5) and (total_time > 1):
				total_time = total_time+3
			elif total_time> 5:
				total_time += 5
	#If we have less than 40 moves, assume that we only need to do half
	#  of them.
	#Iteratively increase the depth and have the estimate of how long it
	#  takes us to go to that depth. This depends on the branching factor.
	else :
		time_per_move = 2* time/(float(moves))
		depth = 0
		bf = calculate_bf(moves, depth)
		total_time = math.pow((float(4)*bf/5), depth)/speed
		if total_time <= 5:
			total_time = total_time +3
		elif total_time > 5:
			total_time += 5
		tmp_depth = 0
		while total_time < time_per_move:
			depth = tmp_depth
			tmp_depth +=1
			bf = calculate_bf(moves, tmp_depth)
			total_time = math.pow((float(4)*bf/5), tmp_depth)/speed
			if (total_time <= 5) and (total_time > 1):
				total_time = total_time +3
			elif total_time > 5:
				t += 5
	return depth

def start_max(board, height, types, time_remaining, speed):
	"""
	Start of the max playing function. This function is similar to
	max_move. The reason the first layer is peeled off is to obtain
	the move corresponding to the max-value of the alpha-beta search.
	:param board: the current game board
	:param height: the length and width of the board
	:param types: the different types of fruits that are present on
	the board
	:param time_remaining: time remaining in the game.
	:param speed: the speed of the computer we are on.
	:return: the number of points scored by the move that maximizes
	 our points, the move itself, and the points scored by max.
	"""
	best_points = -1000000
	max_board = []
	alpha = -1000000
	beta = 1000000
	possible_moves = list(get_possible_moves(board))
	cutoff = get_cutoff(len(possible_moves), time_remaining, speed)
	possible_moves.sort(key_func)
	maxpoints = 0
	for move, points in possible_moves:
		gravity_move = [ row[:] for row in move]
		gravity_move = apply_gravity(gravity_move)
		value = min_move(gravity_move, 0, 0+points,  alpha, beta, 0, cutoff)
		if value > best_points:
			best_points  = value
			max_board = move
			maxpoints = points
		if best_points >= beta:
			return best_points
		alpha = max(best_points, alpha)
	return best_points, max_board, maxpoints

def get_possible_moves(board):
	"""
	Given a board, get all possible moves that can be made on the board.
	This function is written as a generator so that we need not compute
	all possible moves if we are sure we have found the best move.
	:param board: the current game board
	:return: yield a possible move
	"""
	covered = [[ int(elem == '*') for elem in row ]for row in board]
	for row in range(0, len(covered)):
		for column in range(0, len(covered)):
			if covered[row][column] == 1:
				continue
			else:
				queue = []
				count = 0
				new_board = [ l[:] for l in board]
				new_board[row][column] = '*'
				covered[row][column] = 1
				count += 1
				queue.append((row, column))
				while len(queue) > 0:
					(i, j) = queue.pop(0)
					x = i - 1
					if (x > -1) and (board[x][j] == board[i][j]) and (covered[x][j] == 0):
						covered[x][j] = 1
						new_board[x][j] = '*'
						count += 1
						queue.append((x,j))
					x = i + 1
					if (x < len(board)) and (board[x][j] == board[i][j]) and (covered[x][j] == 0):
						covered[x][j] = 1
						new_board[x][j] = '*'
						count += 1
						queue.append((x,j)) 
					y = j + 1
					if (y < len(board)) and (board[i][j] == board[i][y]) and (covered[i][y] == 0):
						covered[i][y] = 1
						new_board[i][y] = '*'
						count += 1
						queue.append((i,y))
					y = j - 1
					if (y > -1) and (board[i][j] == board[i][y]) and (covered[i][y] == 0):
						covered[i][y] = 1
						new_board[i][y] = '*'
						count += 1
						queue.append((i,y))
				yield new_board, int(math.pow(count , 2))

def get_move(board, move):
	"""
	Print the move. The rows are named starting from 0.
	Columns are named starting from A.
	:param board: the current game board
	:param move: the move that was made
	:return: the string that denotes the move
	"""
	n = len(board)
	for row in range(0, n):
		for column in range(0, n):
			if board[row][column] != move[row][column]:
				column_no = ord('A') + column
				row_no = row + 1
				move_str = chr(column_no)+str(row_no)
				return move_str
	


start_time = time.time()

input = open("input.txt","r")
height = int(input.readline())
types = int(input.readline())
time_remaining = float(input.readline())
board = [[cell for cell in line.strip()] for line in input]
input.close()

if game_over(board):
	print "game_over"
	exit(0)
#count the numer of fruits that are present
fruits = {}
for line in board:
	for fruit in line:
		if fruits.get(fruit,None) == None:
			fruits[fruit] = 1
		else:
			fruits[fruit] += 1


new_board = apply_gravity(board)
try:
	calibrate = open("calibration.txt","r")
	speed = float(calibrate.readline())
	calibrate.close()

except FileNotFoundError:
	speed = 50000


value, max_board, points= start_max(new_board, height, types, time_remaining, speed)
move = get_move(new_board, max_board)
apply_gravity(max_board)
end_time = time.time()

time_remaining -= (end_time - start_time)
input = open("input.txt","w")
input.write(str(height)+"\n")
input.write(str(types)+"\n")
input.write(str(time_remaining)+"\n")

for row in max_board:
	line = "".join(row)
	input.write(line)
	input.write("\n")
input.close()

output = open("output.txt","w")
output.write(move+"\n")
for row in max_board:
	line = "".join(row)
	output.write(line+"\n")
output.close()

